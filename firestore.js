import firebase from "firebase";
import '@firebase/firestore'

const getCollectionRef = (collectionName) => firebase.firestore().collection(collectionName);

exports.getCollectionRef = getCollectionRef;
