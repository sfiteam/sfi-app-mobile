import { createStackNavigator, createAppContainer } from 'react-navigation'
import LoginScreen from './screens/Login'
import RegisterScreen from './screens/Registration'
import HomeScreen from './screens/HomeScreen'
import { FontAwesome } from '@expo/vector-icons'
import AgendaScreen from './screens/Agenda'
import SeekFindInputScreen from './screens/SeekFindInput'
import React from 'react'
import WeatherScreen from './screens/Weather'
import FeedbackScreen from './screens/Feedback'
import OrgsScreen from './screens/Orgs'
import OrgsInfoScreen from './screens/OrgsInfo'


const Navigation = createStackNavigator({
  Login: {
    screen: LoginScreen
  },
  Register: {
    screen: RegisterScreen
  },
  Home: {
    screen: HomeScreen
  },
  Agenda: {
    screen: AgendaScreen
  },
  Feedback: {
    screen: FeedbackScreen
  },
  SeekFindInput: {
    screen: SeekFindInputScreen
  },
  Weather: {
    screen: WeatherScreen
  },
  Orgs: {
    screen: OrgsScreen
  },
  OrgsInfo: {
    screen: OrgsInfoScreen
  }
})
export default createAppContainer(Navigation)
