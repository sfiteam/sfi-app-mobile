import React from 'react'
import {View} from 'react-native'
import Navigation from './Navigation'
import ApiKeys from './constants/ApiKeys'
import firebase from 'firebase'

firebase.initializeApp(ApiKeys.FirebaseConfig);

export default class App extends React.Component {
  render () {
    return (
      <View style={{ flex: 1 }}>
        <Navigation />
      </View>
    )
  }
}
