import React from 'react'
import { Image, ListView, Modal, StatusBar, Text, View } from 'react-native'
import { AppLoading } from 'expo'
import {
  Body,
  Button,
  Card,
  CardItem,
  Container,
  Content,
  DatePicker,
  Form,
  Header,
  Icon as BaseIcon,
  Input,
  Item,
  Left,
  Right,
  Footer,
  FooterTab,
  Root,
  Thumbnail,
  Title
} from 'native-base'
import { getCollectionRef } from '../firestore'
import AgendaItemAddButton from './AgendaItemAddButton'

import Icon from 'react-native-vector-icons/FontAwesome'

export default class AgendaScreen extends React.Component {
  static navigationOptions = {
    header: null
  }

  constructor (props) {
    super(props)

    let ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    this.state = {
      text: '',
      description: '',
      speaker: '',
      language: '',
      room: '',
      title: '',
      date: new Date(),
      lectureDate: new Date(),
      isLoadingComplete: false,
      itemDataSource: ds,
      modalVisible: false
    }

    this.renderRow = this.renderRow.bind(this)
    this.pressRow = this.pressRow.bind(this)
    this.setDate = this.setDate.bind(this)
  }

  async componentWillMount () {
    this.collectionRef = getCollectionRef('lectures')
    this.unsubscribe = this.collectionRef.onSnapshot(this.onAgendaUpdate)
    this.setState({ isLoadingComplete: true })
  }

  onAgendaUpdate = querySnapshot => {
    const agenda = []

    querySnapshot.forEach(doc => {
      agenda.push({ ...doc.data(), _key: doc.id })
    })

    this.setState({
      itemDataSource: this.state.itemDataSource.cloneWithRows(agenda)
    })
  }

  componentWillUnmount () {
    this.unsubscribe()
  }

  setModalVisible (visible) {
    this.setState({ modalVisible: visible })
  }

  setDate (newDate) {
    this.setState({ lectureDate: newDate })
  }

  async pressRow (item) {
    await this.collectionRef.doc(item._key).delete()
  }

  renderRow (item) {
    return (
      <Content>
        <Card>
          <CardItem>
            <Left>
              <Thumbnail
                source={{
                  uri: 'https://sfi.pl/wp-content/uploads/2019/02/logo.png'
                }}
              />
              <Body>
                <Text>{item.title}</Text>
                <Text note>{item.speaker}</Text>
              </Body>
            </Left>
          </CardItem>
          <CardItem cardBody>
            <Image
              source={{
                uri:
                  'https://sfi.pl/wp-content/uploads/2019/02/sfi-15-motyw.png'
              }}
              style={{ height: 200, width: null, flex: 1 }}
            />
          </CardItem>
          <CardItem>
            <Text>{item.description}</Text>
          </CardItem>
          <CardItem>
            <Left>
              <Button transparent>
                <Text>Room: {item.room}</Text>
              </Button>
            </Left>
            <Body>
              <Button transparent>
                <Text>Language: {item.language}</Text>
              </Button>
            </Body>
          </CardItem>
          <CardItem>
            <Left>
              <Button
                color='#FFB6C1'
                rounded
                onPress={() => {
                  this.props.navigation.navigate('Feedback', { item: item })
                }}
              >
                <BaseIcon name='star' />
              </Button>
            </Left>
            <Right>
              <Button
                danger
                rounded
                onPress={() => {
                  this.pressRow(item)
                }}
              >
                <BaseIcon name='trash' />
              </Button>
            </Right>
          </CardItem>
        </Card>
      </Content>
    )
  }

  addItem () {
    this.setModalVisible(true)
  }

  render () {
    const { goBack } = this.props.navigation
    if (!this.state.isLoadingComplete) {
      return (
        <Root>
          <AppLoading />
        </Root>
      )
    }
    return (
      <Container>
        <View>
          <Modal
            animationType='slide'
            transparent={false}
            visible={this.state.modalVisible}
            onRequestClose={() => {}}
          >
            <Form>
              <Item>
                <Input
                  value={this.state.title}
                  placeholder='Title'
                  onChangeText={value => this.setState({ title: value })}
                />
              </Item>
              <Item>
                <Input
                  value={this.state.description}
                  placeholder='Description'
                  onChangeText={value => this.setState({ description: value })}
                />
              </Item>
              <Item>
                <Input
                  value={this.state.speaker}
                  placeholder='Speaker'
                  onChangeText={value => this.setState({ speaker: value })}
                />
              </Item>
              <Item>
                <Input
                  value={this.state.room}
                  placeholder='Room'
                  onChangeText={value => this.setState({ room: value })}
                />
              </Item>
              <Item>
                <DatePicker
                  defaultDate={new Date(2018, 4, 4)}
                  minimumDate={new Date(2018, 1, 1)}
                  maximumDate={new Date(2030, 12, 31)}
                  locale={'en'}
                  timeZoneOffsetInMinutes={undefined}
                  modalTransparent={false}
                  animationType={'fade'}
                  androidMode={'default'}
                  placeholder='Select date'
                  textStyle={{ color: 'green' }}
                  onDateChange={this.setDate}
                  disabled={false}
                />
              </Item>
              <Item last>
                <Input
                  value={this.state.language}
                  placeholder='Language'
                  onChangeText={value => this.setState({ language: value })}
                />
              </Item>
            </Form>
            <Button
              success
              full
              rounded
              style={{ marginTop: 25 }}
              onPress={async () => {
                await this.collectionRef.add({
                  title: this.state.title,
                  description: this.state.description,
                  language: this.state.language,
                  room: this.state.room,
                  speaker: this.state.speaker,
                  date: this.state.lectureDate,
                  rating: []
                })
                this.setModalVisible(!this.state.modalVisible)
              }}
            >
              <Text>Zapisz</Text>
            </Button>
            <Button
              danger
              full
              rounded
              style={{ marginTop: 10 }}
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible)
              }}
            >
              <Text>Anuluj</Text>
            </Button>
          </Modal>
        </View>
        <Header style={{ marginTop: StatusBar.currentHeight }}>
          <Left>
            <Icon
              name='rocket'
              size={30}
              color='white'
              onPress={() => goBack()}
            />
          </Left>
          <Body>
            <Title style={{ paddingLeft: 10 }}>Agenda SFI</Title>
          </Body>
          <Right>
            <AgendaItemAddButton onPress={this.addItem.bind(this)} />
          </Right>
        </Header>
        <ListView
          dataSource={this.state.itemDataSource}
          renderRow={this.renderRow}
        />
        <Footer>
          <FooterTab>
            <Button>
              <BaseIcon
                type='Entypo'
                name='book'
                size={35}
                color='white'
                onPress={props => this.props.navigation.navigate('Agenda')}
              />
            </Button>
            <Button>
              <BaseIcon
                type='Ionicons'
                name='ios-person-add'
                size={35}
                color='white'
                onPress={props => this.props.navigation.navigate('Orgs')}
              />
            </Button>
            <Button>
              <BaseIcon
                type='FontAwesome'
                name='qrcode'
                size={35}
                color='white'
                onPress={props =>
                  this.props.navigation.navigate('SeekFindInput')
                }
              />
            </Button>
            <Button>
              <BaseIcon
                type='Ionicons'
                name='ios-rainy'
                size={35}
                color='white'
                onPress={props =>
                  this.props.navigation.navigate('Weather', {
                    weather: this.state.weatherCondition,
                    temperature: this.state.temperature
                  })
                }
              />
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    )
  }
}
