import React from 'react';
import {Picker, StyleSheet, View} from 'react-native';
import {Button, Card, CardItem, Container, Content, Icon, Text} from 'native-base';
import firebase from 'firebase';

export default class FeedbackScreen extends React.Component {


  constructor(props) {
    super(props);

    this.state = {
      agendaItem: '',
      rating: 1,
      rates: []
    }
  }

  componentDidMount() {
    this.setState({agendaItem: this.props.navigation.state.params.item});
    this.state.rates = this.state.agendaItem.rating;
  }


  render() {
    return (
      <Container>
        <Content padder>
          <Card>
            <View style={styles.container}>
              <CardItem header bordered>
                <Text>Jak Ci się podobał wykład "{this.state.agendaItem.title}"</Text>
              </CardItem>
              <CardItem>
                <Text>Twój wybór: {this.state.rating}</Text>
                <Picker
                  style={{position: 'absolute', bottom: 0, left: 0, right: 0}}
                  selectedValue={this.state.rating}
                  style={{height: 50, width: 200}}
                  onValueChange={(itemValue, itemIndex) => {
                    this.setState({rating: itemValue});
                  }
                  }>
                  <Picker.Item label="Rating: 1" value='1'/>
                  <Picker.Item label="Rating: 2" value='2'/>
                  <Picker.Item label="Rating: 3" value='3'/>
                  <Picker.Item label="Rating: 4" value='4'/>
                  <Picker.Item label="Rating: 5" value='5'/>
                </Picker>
              </CardItem>
              <CardItem footer>
                <Button
                  primary
                  rounded
                  style={{marginTop: 25}}
                  onPress={() => {
                    this.state.agendaItem.rating.push(parseInt(this.state.rating));

                    firebase.firestore().collection('lectures')
                      .doc(this.state.agendaItem._key)
                      .update({rating: this.state.agendaItem.rating});


                    this.props.navigation.navigate('Agenda');
                  }}
                >
                  <Icon name='add'/>
                </Button>
                <Button
                  danger
                  rounded
                  style={{marginTop: 25}}
                  onPress={() => {
                    this.props.navigation.navigate('Agenda');
                  }}
                >
                  <Icon name='arrow-back'/>
                </Button>
              </CardItem>
            </View>
          </Card>
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
