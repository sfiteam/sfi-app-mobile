import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {BarCodeScanner, Constants, Permissions} from 'expo';
import {getCollectionRef} from '../firestore'

export default class SeekFindInputScreen extends React.Component {

  state = Object.assign(
    {
      hasCameraPermission: null
    });

  async componentWillMount() {
    this.collectionRef = getCollectionRef('seekFindInputCodes');
  }


  componentDidMount() {
    this._requestCameraPermission();
  };

  _requestCameraPermission = async () => {
    const {status} = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({
      hasCameraPermission: status === 'granted',
    });
  };

  _handleBarCodeRead = data => {
    let docRef = this.collectionRef.doc(data.data);
    docRef.get().then(function (doc) {
      if (doc.exists) {
        let data = doc.data();
        alert("ID kodu: " + doc.id +
          "\nCzy kod zużyty: " + (data.used ? "Tak" : "Nie") +
          "\nCzy kod aktywny: " + (data.active ? "Tak" : "Nie") +
          "\nPunkty: " + data.points +
          "\nKomentarz: " + data.comment
        );
      } else {
        alert("Nie odnaleziono kodu");
      }
    }).catch(function (error) {
      alert("Błąd podczas pobierania danych:" + error);
    });
  };

  render() {
    return (
      <View style={styles.container}>
        {this.state.hasCameraPermission === null ?
          <Text>Wymagane uprawnienia do aparatu</Text> :
          this.state.hasCameraPermission === false ?
            <Text>Brak uprawnień do aparatu</Text> :
            (<Text>Zeskanuj kod, aby uzyskać informacj:!</Text>,
              <BarCodeScanner
                onBarCodeRead={this._handleBarCodeRead}
                style={{height: '100%', width: '100%'}}
              />)
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  }
});
