import React from 'react'
import { getCollectionRef } from '../firestore'
import { AppLoading } from 'expo'
import Icon from 'react-native-vector-icons/FontAwesome'
import {
  Body,
  Button,
  Container,
  Content,
  Footer,
  FooterTab,
  Root,
  Header,
  Text,
  Title,
  Icon as BaseIcon,
  Left,
  Right
} from 'native-base'
import { StatusBar, View, StyleSheet } from 'react-native'
import t from 'tcomb-form-native'

const Form = t.form.Form

const Org = t.struct({
  name: t.String,
  surname: t.String,
  phone: t.Integer,
  team: t.String
})

export default class OrgsScreen extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      isLoadingComplete: false,
      value: null
    }
  }

  static navigationOptions = {
    header: null
  }
  async componentWillMount () {
    this.collectionRef = getCollectionRef('organizators')
    this.setState({ isLoadingComplete: true })
  }

  handleSubmit = () => {
    const value = this._form.getValue()
    this.collectionRef.add({
      name: value.name,
      surname: value.surname,
      tel: value.phone,
      team: value.team
    })
    this.clearForm()
  }

  getInitialState () {
    return { value: null }
  }

  onChange (value) {
    this.setState({ value })
  }

  clearForm () {
    this.setState({ value: null })
  }

  render () {
    const { goBack } = this.props.navigation
    if (!this.state.isLoadingComplete) {
      return (
        <Root>
          <AppLoading />
        </Root>
      )
    } else {
      return (
        <Container>
          <Header style={{ marginTop: StatusBar.currentHeight }}>
            <Left>
              <Icon
                name='rocket'
                size={30}
                color='white'
                onPress={() => goBack()}
              />
            </Left>
            <Body>
              <Title style={{ paddingLeft: 10 }}>Dodaj Orga!</Title>
            </Body>
            <Right />
          </Header>
          <Content>
            <View style={styles.container}>
              <Form
                ref={c => (this._form = c)}
                value={this.state.value}
                type={Org}
                onChange={this.onChange.bind(this)}
              />
              <Button
                info
                full
                rounded
                style={{ marginTop: 10 }}
                onPress={this.handleSubmit}
              >
                <Text>Dodaj</Text>
              </Button>
            </View>
          </Content>
          <Footer>
            <FooterTab>
              <Button>
                <BaseIcon
                  type='Entypo'
                  name='book'
                  size={35}
                  color='white'
                  onPress={props => this.props.navigation.navigate('Agenda')}
                />
              </Button>
              <Button>
                <BaseIcon
                  type='Ionicons'
                  name='ios-person-add'
                  size={35}
                  color='white'
                  onPress={props => this.props.navigation.navigate('Orgs')}
                />
              </Button>
              <Button>
                <BaseIcon
                  type='FontAwesome'
                  name='qrcode'
                  size={35}
                  color='white'
                  onPress={props =>
                    this.props.navigation.navigate('SeekFindInput')
                  }
                />
              </Button>
              <Button>
                <BaseIcon
                  type='Ionicons'
                  name='ios-rainy'
                  size={35}
                  color='white'
                  onPress={props =>
                    this.props.navigation.navigate('Weather', {
                      weather: this.state.weatherCondition,
                      temperature: this.state.temperature
                    })
                  }
                />
              </Button>
            </FooterTab>
          </Footer>
        </Container>
      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    marginTop: 50,
    padding: 20,
    backgroundColor: '#ffffff'
  }
})
