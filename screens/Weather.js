import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import PropTypes from 'prop-types';
import { weatherConditions } from '../constants/WeatherConditions';

export default class WeatherScreen extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            weather: '',
            temperature: ''
        }
    }

    componentWillMount() {
        this.setState({
            weather: this.props.navigation.state.params.weather,
            temperature: this.props.navigation.state.params.temperature});
    }

    render() {
        return (
            <View
            style={[
                styles.weatherContainer,
                { backgroundColor: weatherConditions[this.state.weather].color }
            ]}
            >
            <View style={styles.headerContainer}>
                <MaterialCommunityIcons
                size={72}
                name={weatherConditions[this.state.weather].icon}
                color={'#fff'}
                />
                <Text style={styles.tempText}>{this.state.temperature}˚</Text>
            </View>
            <View style={styles.bodyContainer}>
                <Text style={styles.title}>{weatherConditions[this.state.weather].title}</Text>
                <Text style={styles.subtitle}>
                {weatherConditions[this.state.weather].subtitle}
                </Text>
            </View>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    weatherContainer: {
      flex: 1
    },
    headerContainer: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-around'
    },
    tempText: {
      fontSize: 72,
      color: '#fff'
    },
    bodyContainer: {
      flex: 2,
      alignItems: 'flex-start',
      justifyContent: 'flex-end',
      paddingLeft: 25,
      marginBottom: 40
    },
    title: {
      fontSize: 60,
      color: '#fff'
    },
    subtitle: {
      fontSize: 24,
      color: '#fff'
    }
  });