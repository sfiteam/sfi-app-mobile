import React from 'react';
import {material} from 'react-native-typography';
import {StyleSheet, Text, View} from 'react-native';
import {AppLoading, Font} from 'expo';
import firebase from 'firebase';
import {Ionicons} from '@expo/vector-icons';
import {Button, Container, Form, Icon, Input, Item, Label, Root} from 'native-base';
import {navigation} from 'react-navigation';

export default class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoadingComplete: false,
      email: '',
      password: ''
    }
  }

  static navigationOptions = {
    header: null
  };

  async componentWillMount() {

    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font
    });
    this.setState({isLoadingComplete: true})
  }

  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (user != null) {
        this.props.navigation.navigate('Home')
      }
    })
  }

  LogIn = (email, password) => {
    try {
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(res => {
          console.log(this.state.userData);
          this.props.navigation.navigate('Home')
        })
    } catch (error) {
      alert('Login Failed. Please try again' + error)
    }
  };

  async loginWithFacebook() {
    const {type, token} = await Expo.Facebook.logInWithReadPermissionsAsync(
      '401322023789734',
      {permissions: ['public_profile']}
    );

    if (type === 'success') {

      const credential = firebase.auth.FacebookAuthProvider.credential(token);

      const data = await firebase.auth().signInAndRetrieveDataWithCredential(credential);
      firebase.database().ref().child('users/').push({login: data.user.displayName});
      console.log(data);
      this.props.navigation.navigate('Home')
    }
  }

  render() {
    if (!this.state.isLoadingComplete) {
      return (
        <Root>
          <AppLoading/>
        </Root>
      )
    }
    return (
      <Container>
        <View style={styles.main}>
          <Form style={{paddingBottom: 10}}>
            <Item floatingLabel>
              <Icon name='ios-person'/>
              <Label>Email</Label>
              <Input
                autoCapitalize='none'
                autoCorrect={false}
                onChangeText={email => this.setState({email})}
              />
            </Item>
            <Item floatingLabel>
              <Icon name='ios-unlock'/>
              <Label>Hasło</Label>
              <Input
                secureTextEntry
                autoCapitalize='none'
                autoCorrect={false}
                onChangeText={password => this.setState({password})}
              />
            </Item>
            <Button
              success
              full
              rounded
              style={{marginTop: 25}}
              onPress={() => this.LogIn(this.state.email, this.state.password)}
            >
              <Text style={styles.title}>Zaloguj się</Text>
            </Button>
            <Button
              style={{marginTop: 10}}
              primary
              full
              rounded
              onPress={() => this.loginWithFacebook()}
            >
              <Text style={styles.title}>Zaloguj przez Facebook</Text>
            </Button>
          </Form>
          <Text style={[material.caption, styles.title1]}>
            Nie masz konta?{' '}
            <Text
              style={styles.title2}
              onPress={props => this.props.navigation.navigate('Register')}
            >
              {' '}
              Zarejestruj się!
            </Text>
          </Text>
        </View>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  main: {
    justifyContent: 'center',
    flex: 1
  },
  title: {
    color: 'white'
  },
  title1: {
    textAlign: 'center',
    paddingTop: 10
  },
  title2: {
    fontWeight: 'bold'
  }
});
