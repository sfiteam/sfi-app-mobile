import React from 'react'
import { getCollectionRef } from '../firestore'
import { AppLoading } from 'expo'
import Icon from 'react-native-vector-icons/FontAwesome'
import { List, ListItem } from 'react-native-elements'
import {
  Body,
  Button,
  Container,
  Content,
  Footer,
  FooterTab,
  Root,
  Header,
  Text,
  Title,
  Icon as BaseIcon,
  Left,
  Right
} from 'native-base'
import {
  StatusBar,
  View,
  StyleSheet,
  ListView,
  TouchableOpacity
} from 'react-native'
import {
  Divider,
  ImageBackground,
  Screen,
  Subtitle,
  Tile,
  Heading
} from '@shoutem/ui'

export default class OrgsInfoScreen extends React.Component {
  constructor (props) {
    super(props)
    const orgs = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    this.state = {
      isLoadingComplete: false,
      dataSource: orgs
    }
    this.renderRow = this.renderRow.bind(this)
  }
  static navigationOptions = {
    header: null
  }
  async componentWillMount () {
    this.collectionRef = getCollectionRef('organizators')
    this.unsubscribe = this.collectionRef.onSnapshot(this.onOrgsUpdate)
    this.setState({ isLoadingComplete: true })
  }

  onOrgsUpdate = querySnapshot => {
    const orgs = []

    querySnapshot.forEach(doc => {
      orgs.push({ ...doc.data(), _key: doc.id })
    })

    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(orgs)
    })
  }

  componentWillUnmount () {
    this.unsubscribe()
  }

  renderRow (item) {
    return (
      <TouchableOpacity>
        <ImageBackground styleName='large' source={{ uri: item.image }}>
          <Tile>
            <Title styleName='md-gutter-bottom'>{item.name}</Title>
            <Subtitle styleName='sm-gutter-horizontal'>{item.surname}</Subtitle>
            <Heading>{item.tel}</Heading>
            <Heading>{item.team}</Heading>
          </Tile>
        </ImageBackground>
        <Divider styleName='line' />
      </TouchableOpacity>
    )
  }

  render () {
    const { goBack } = this.props.navigation
    if (!this.state.isLoadingComplete) {
      return (
        <Root>
          <AppLoading />
        </Root>
      )
    } else {
      return (
        <Container>
          <Header style={{ marginTop: StatusBar.currentHeight }}>
            <Left>
              <Icon
                name='rocket'
                size={30}
                color='white'
                onPress={() => goBack()}
              />
            </Left>
            <Body>
              <Title style={{ paddingLeft: 10 }}>Organizatorzy</Title>
            </Body>
            <Right />
          </Header>
          <Content>
            <Screen>
              <ListView
                dataSource={this.state.dataSource}
                renderRow={this.renderRow}
              />
            </Screen>
          </Content>
          <Footer>
            <FooterTab>
              <Button>
                <BaseIcon
                  type='Entypo'
                  name='book'
                  size={35}
                  color='white'
                  onPress={props => this.props.navigation.navigate('Agenda')}
                />
              </Button>
              <Button>
                <BaseIcon
                  type='Ionicons'
                  name='ios-person-add'
                  size={35}
                  color='white'
                  onPress={props => this.props.navigation.navigate('Orgs')}
                />
              </Button>
              <Button>
                <BaseIcon
                  type='FontAwesome'
                  name='qrcode'
                  size={35}
                  color='white'
                  onPress={props =>
                    this.props.navigation.navigate('SeekFindInput')
                  }
                />
              </Button>
              <Button>
                <BaseIcon
                  type='Ionicons'
                  name='ios-rainy'
                  size={35}
                  color='white'
                  onPress={props =>
                    this.props.navigation.navigate('Weather', {
                      weather: this.state.weatherCondition,
                      temperature: this.state.temperature
                    })
                  }
                />
              </Button>
            </FooterTab>
          </Footer>
        </Container>
      )
    }
  }
}
