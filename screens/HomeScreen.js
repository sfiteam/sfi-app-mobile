import { navigation } from 'react-navigation'
import React from 'react'
import { Font } from 'expo'
import Menu, { MenuDivider, MenuItem } from 'react-native-material-menu'
import {
  ActivityIndicator,
  ListView,
  StatusBar,
  TouchableOpacity
} from 'react-native'
import {
  Body,
  Button,
  Container,
  Footer,
  FooterTab,
  Header,
  Icon,
  Left,
  Right,
  Title
} from 'native-base'
import { Divider, ImageBackground, Screen, Subtitle, Tile } from '@shoutem/ui'
import firebase from 'firebase'
import { getCollectionRef } from '../firestore'

console.disableYellowBox = true // disable warnings
const API_KEY = '83663f86f4867dec9d933c0376770a8c'

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null
  }
  _menu = null

  constructor (props) {
    super(props)
    const dataNews = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    this.state = {
      users: [],
      logged_user: null,
      loading: true,
      fontsAreLoaded: false,
      dataSource: dataNews,

      temperature: 0,
      weatherCondition: null,
      error: null
    }
    this.renderRow = this.renderRow.bind(this)
  }

  async componentWillMount () {
    this.collectionRef = getCollectionRef('news')
    this.unsubscribe = this.collectionRef.onSnapshot(this.onNewsUpdate)
    await Font.loadAsync({
      'Rubik-Regular': require('../node_modules/@shoutem/ui/fonts/Rubik-Regular.ttf')
    })
    this.setState({ fontsAreLoaded: true })
  }

  fetchWeather (lat = 25, lon = 25) {
    fetch(
      `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&APPID=${API_KEY}&units=metric`
    )
      .then(res => res.json())
      .then(json => {
        this.setState({
          temperature: json.main.temp,
          weatherCondition: json.weather[0].main,
          loading: false
        })
      })
  }

  onNewsUpdate = querySnapshot => {
    const news = []

    querySnapshot.forEach(doc => {
      news.push({ ...doc.data(), _key: doc.id })
    })

    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(news)
    })
  }

  componentDidMount () {
    firebase.auth().onAuthStateChanged(logged_user => {
      if (logged_user != null) {
        this.setState({
          logged_user: logged_user
        })
      }
    })

    navigator.geolocation.getCurrentPosition(
      position => {
        this.fetchWeather(position.coords.latitude, position.coords.longitude)
        this.setState({
          loading: false
        })
      },
      error => {
        this.setState({
          error: 'Error Gettig Weather Condtions'
        })
      }
    )
  }

  componentWillUnmount () {
    this.unsubscribe()
  }

  signOutUser = async () => {
    try {
      await firebase.auth().signOut()
      this.props.navigation.navigate('Login')
    } catch (e) {
      console.log(e)
    }
  }

  setMenuRef = ref => {
    this._menu = ref
  }

  hideMenu = () => this._menu.hide()

  showMenu = () => this._menu.show()

  renderRow (item) {
    return (
      <TouchableOpacity>
        <ImageBackground styleName='large' source={{ uri: item.image }}>
          <Tile>
            <Title styleName='md-gutter-bottom'>{item.name}</Title>
            <Subtitle styleName='sm-gutter-horizontal'>{item.date}</Subtitle>
          </Tile>
        </ImageBackground>
        <Divider styleName='line' />
      </TouchableOpacity>
    )
  }

  render () {
    const main_content = this.state.loading

    if (main_content) {
      return <ActivityIndicator size='large' />
    }
    if (!this.state.fontsAreLoaded) {
      return <ActivityIndicator size='large' />
    } else {
      return (
        <Container>
          <Header style={{ marginTop: StatusBar.currentHeight }}>
            <Left>
              <Menu
                ref={this.setMenuRef}
                button={
                  <Button transparent>
                    <Icon onPress={this.showMenu} name='menu' />
                  </Button>
                }
              >
                <MenuItem onPress={this.hideMenu}>Profil</MenuItem>
                <MenuItem onPress={this.hideMenu}>Ustawienia</MenuItem>
                <MenuItem onPress={this.hideMenu} disabled>
                  Pomoc
                </MenuItem>
                <MenuDivider />
                <MenuItem onPress={this.signOutUser}>Wyloguj się</MenuItem>
              </Menu>
            </Left>
            <Body>
              <Title>SFI</Title>
            </Body>
            <Right>
              <Button transparent>
                <Icon
                  name='person'
                  onPress={props => this.props.navigation.navigate('OrgsInfo')}
                />
              </Button>
            </Right>
          </Header>
          <Screen>
            <ListView
              dataSource={this.state.dataSource}
              renderRow={this.renderRow}
            />
          </Screen>
          <Footer>
            <FooterTab>
              <Button>
                <Icon
                  type='Entypo'
                  name='book'
                  size={35}
                  color='white'
                  onPress={props => this.props.navigation.navigate('Agenda')}
                />
              </Button>
              <Button>
                <Icon
                  type='Ionicons'
                  name='ios-person-add'
                  size={35}
                  color='white'
                  onPress={props => this.props.navigation.navigate('Orgs')}
                />
              </Button>
              <Button>
                <Icon
                  type='FontAwesome'
                  name='qrcode'
                  size={35}
                  color='white'
                  onPress={props =>
                    this.props.navigation.navigate('SeekFindInput')
                  }
                />
              </Button>
              <Button>
                <Icon
                  type='Ionicons'
                  name='ios-rainy'
                  size={35}
                  color='white'
                  onPress={props =>
                    this.props.navigation.navigate('Weather', {
                      weather: this.state.weatherCondition,
                      temperature: this.state.temperature
                    })
                  }
                />
              </Button>
            </FooterTab>
          </Footer>
        </Container>
      )
    }
  }
}
