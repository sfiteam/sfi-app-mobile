import React from 'react';
import {Text, View} from 'react-native';
import {AppLoading, Font} from 'expo';
import firebase from 'firebase';
import {Ionicons} from '@expo/vector-icons';
import {Button, Container, Form, Input, Item, Label, Root} from 'native-base';

export default class RegisterScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoadingComplete: false,
      email: '',
      password: ''
    }
  }

  SignUp = (email, password) => {
    try {
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then(user =>
          firebase.database().ref().child('users/').push({login: this.state.email})
        )
    } catch (error) {
      console.log(error.toString(error))
    }
  };

  async componentWillMount() {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font
    });
    this.setState({isLoadingComplete: true})
  }

  render() {
    if (!this.state.isLoadingComplete) {
      return (
        <Root>
          <AppLoading/>
        </Root>
      )
    }
    return (
      <Container>
        <View style={{justifyContent: 'center', flex: 1}}>
          <Form style={{paddingBottom: 10}}>
            <Item floatingLabel>
              <Label>Email</Label>
              <Input
                autoCapitalize='none'
                autoCorrect={false}
                onChangeText={email => this.setState({email})}
              />
            </Item>
            <Item floatingLabel>
              <Label>Hasło</Label>
              <Input
                secureTextEntry
                autoCapitalize='none'
                autoCorrect={false}
                onChangeText={password => this.setState({password})}
              />
            </Item>
          </Form>
          <Button
            info
            full
            rounded
            style={{marginTop: 10}}
            onPress={() => this.SignUp(this.state.email, this.state.password)}
          >
            <Text>Zarejestruj się</Text>
          </Button>
        </View>
      </Container>
    )
  }
}
