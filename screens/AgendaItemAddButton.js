import React from 'react';
import {Text} from 'react-native';
import {Button, Icon} from 'native-base';

export default class AgendaItemAddButton extends React.Component {
  render() {
    return (
      <Button rounded onPress={this.props.onPress}>
        <Icon name='add'/>
        <Text>{this.props.title}</Text>
      </Button>
    )
  }
}
